import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as mongoose from 'mongoose';

async function bootstrap() {
  // fix deprecation notice
  mongoose.set('useNewUrlParser', true);
  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);
  mongoose.set('useUnifiedTopology', true);

  const whitelist = [
    'http://localhost',
    'http://localhost:8080',
  ];
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: function (origin, callback) {
        if (!origin || whitelist.indexOf(origin) !== -1) {
          console.log(`allowed cors for: ${origin}`);
          callback(null, true);
        } else {
          console.log(`blocked cors for: ${origin}`);
          callback(new Error('Not allowed by CORS'));
        }
      },
      allowedHeaders:
        'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Observe',
      methods: 'GET,PUT,PATCH,POST,DELETE,UPDATE,OPTIONS',
      credentials: true,
    },
  });
  await app.listen(80);
  console.log(`Application is running on: ${await app.getUrl()}`);
}

bootstrap();
