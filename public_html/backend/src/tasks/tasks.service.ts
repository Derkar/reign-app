import { Injectable, Logger } from '@nestjs/common';
import { Cron, Interval } from '@nestjs/schedule';
import { PostsService } from '../posts/posts.service';
import { HttpService } from "@nestjs/axios";

@Injectable()
export class TasksService {
  constructor(
      private postsService: PostsService,
      private httpService: HttpService
  ) {}

  private readonly logger = new Logger(TasksService.name);

  async insertData() {
    this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').subscribe(
        async response => {
          this.logger.debug(`GET promise fulfilled (posts recovered from HN)`);
          if (response.data.hits.length > 0) {
            for (const post of response.data.hits) {
              try {
                const saveResponse = await this.postsService.create(post);
                if (saveResponse) {
                  this.logger.debug(
                      `The post with objectID ${post.objectID} was successfully saved`,
                  );
                }
              } catch (e) {
                this.logger.error(e);
              }

            }
          } else {
            this.logger.debug(`no posts were detected`);
          }
        }
    );
  }

  @Cron(new Date(Date.now() + 5 * 1000))
  async populateDB() {
    this.logger.debug('populateDB - Called 5 seconds after startup ');
    this.logger.debug(`Making GET Request...`);
    await this.insertData();
  }

  @Interval(60 * 60 * 1000)
  async refreshDB() {
    this.logger.debug('refreshDB - Called every hour');
    this.logger.debug(`Making GET Request...`);
    await this.insertData();
  }
}
