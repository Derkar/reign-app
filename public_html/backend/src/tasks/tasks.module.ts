import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { PostsModule } from '../posts/posts.module';
import {HttpModule} from "@nestjs/axios";

@Module({
  imports: [HttpModule, PostsModule],
  providers: [TasksService],
})
export class TasksModule {}
