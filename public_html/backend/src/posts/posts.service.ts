import { Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post, PostDocument } from './schemas/post.schema';

@Injectable()
export class PostsService {
  constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {}

  create(createPostDto: CreatePostDto): Promise<Post> {
    return this.postModel.create(createPostDto);
  }

  findAll(): Promise<Post[]> {
    return this.postModel.find().exec();
  }

  findOne(id: string): Promise<Post> {
    return this.postModel.findOne({ _id: id }).exec();
  }

  patch(id: string, updatePostDto: UpdatePostDto): Promise<Post> {
    return this.postModel.findOneAndUpdate({ _id: id }, updatePostDto).exec();
  }

  update(id: string, updatePostDto: UpdatePostDto): Promise<Post> {
    return this.postModel.findOneAndUpdate({ _id: id }, updatePostDto).exec();
  }

  remove(id: string): Promise<Post> {
    return this.postModel.findOneAndDelete({ _id: id }).exec();
  }
}
