import { postStub } from '../tests/stubs/post.stub';

export const PostsRepository = jest.fn().mockReturnValue({
  create: jest.fn().mockResolvedValue(postStub()),
  find: jest.fn().mockResolvedValue(postStub()),
  findOne: jest.fn().mockResolvedValue(postStub()),
  exec: jest.fn().mockResolvedValue(postStub()),
});
