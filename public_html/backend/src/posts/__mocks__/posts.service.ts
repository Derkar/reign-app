import { postStub } from '../tests/stubs/post.stub';

export const PostsService = jest.fn().mockReturnValue({
  create: jest.fn().mockResolvedValue(postStub()),
  findAll: jest.fn().mockResolvedValue([postStub()]),
  findOne: jest.fn().mockResolvedValue(postStub()),
  patch: jest.fn().mockResolvedValue(postStub()),
  update: jest.fn().mockResolvedValue(postStub()),
  remove: jest.fn().mockResolvedValue(postStub()),
});
