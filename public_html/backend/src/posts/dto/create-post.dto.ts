export class CreatePostDto {
  status: string;
  modified_at: string;
  created_at: string;
  title: string;
  author: string;
  url: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: {
    author: {
      value: string;
      matchLevel: string;
      matchedWords: string[];
    };
    comment_text: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: string[];
    };
    story_title: {
      value: string;
      matchLevel: string;
      matchedWords: string[];
    };
    story_url: {
      value: string;
      matchLevel: string;
      matchedWords: string[];
    };
  };
}
