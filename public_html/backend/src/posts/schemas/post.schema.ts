import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ default: 'created' })
  status: string;

  @Prop()
  modified_at: string;

  @Prop()
  created_at: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: number;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop()
  _tags: string[];

  @Prop({ required: true, unique: true, _id: true })
  objectID: string;

  @Prop(
    raw({
      author: {
        value: String,
        matchLevel: String,
        matchedWords: [String],
      },
      comment_text: {
        value: String,
        matchLevel: String,
        fullyHighlighted: Boolean,
        matchedWords: [String],
      },
      story_title: {
        value: String,
        matchLevel: String,
        matchedWords: [String],
      },
      story_url: {
        value: String,
        matchLevel: String,
        matchedWords: [String],
      },
    }),
  )
  _highlightResult: Record<string, any>;
}

export const PostSchema = SchemaFactory.createForClass(Post);
