import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  async create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto).catch((rej) => {
      throw new HttpException(
        {
          //The populate.js script will keep running until it is stopped. This means the server might
          //receive multiple duplicates of the same document.
          //For this instance, objectID is being treated as the primary key.
          //In case the server receives a duplicate document, an exception will be thrown.
          //I've chosen the status code 409 Conflict, because it fits better to this scenario.
          error: HttpStatus.CONFLICT,
          message: 'The document already exists.',
          detail: rej,
        },
        HttpStatus.CONFLICT,
      );
    });
  }

  @Get()
  async findAll() {
    return this.postsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.postsService.findOne(id);
  }

  @Patch(':id')
  async patch(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.patch(id, updatePostDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.update(id, updatePostDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.postsService.remove(id);
  }
}
