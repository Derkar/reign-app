import { Post } from '../../schemas/post.schema';

export const postStub = (): Post => {
  return {
    _highlightResult: {
      author: { matchLevel: 'full', matchedWords: ['nodejs'], value: 'nodejs' },
      comment_text: {
        fullyHighlighted: false,
        matchLevel: 'full',
        matchedWords: ['nodejs'],
        value: 'nodejs',
      },
      story_title: {
        matchLevel: 'full',
        matchedWords: ['nodejs'],
        value: 'nodejs',
      },
      story_url: {
        matchLevel: 'full',
        matchedWords: ['nodejs'],
        value: 'nodejs',
      },
    },
    _tags: ['nodejs', 'lasagna'],
    author: 'Me',
    comment_text: 'asd',
    created_at: 'yesterday',
    created_at_i: 123,
    modified_at: 'today',
    num_comments: 123,
    parent_id: 222,
    points: 12,
    status: 'created',
    story_id: 333,
    story_text: 'ffgfas',
    story_title: 'fffdsa',
    story_url: 'https://www.google.cl/',
    title: 'holanda que talca',
    url: 'https://www.google.cl/',
    objectID: '1233123',
  };
};
