import { MockModel } from '../../../database/tests/support/mock.model';
import { Post } from '../../schemas/post.schema';
import { postStub } from '../stubs/post.stub';

export class PostModel extends MockModel<Post> {
  protected entityStub = postStub();
}
