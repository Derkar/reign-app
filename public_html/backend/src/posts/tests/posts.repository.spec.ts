import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { FilterQuery } from 'mongoose';
import { Post, PostDocument } from '../schemas/post.schema';
import { PostsRepository } from '../posts.repository';
import { postStub } from './stubs/post.stub';
import { PostModel } from './support/posts.model';

describe('PostsRepository', () => {
  let postsRepository: PostsRepository;

  describe('find operations', () => {
    let postModel: PostModel;
    let postFilterQuery: FilterQuery<PostDocument>;

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          PostsRepository,
          {
            provide: getModelToken(Post.name),
            useClass: PostModel,
          },
        ],
      }).compile();

      postsRepository = moduleRef.get<PostsRepository>(PostsRepository);
      postModel = moduleRef.get<PostModel>(getModelToken(Post.name));

      postFilterQuery = {
        objectID: postStub().objectID,
      };

      jest.clearAllMocks();
    });

    describe('findOne', () => {
      describe('when findOne is called', () => {
        let post: Post;

        beforeEach(async () => {
          jest.spyOn(postModel, 'findOne');
          post = await postsRepository.findOne(postFilterQuery);
        });

        test('then it should call the postModel', () => {
          expect(postModel.findOne).toHaveBeenCalledWith(postFilterQuery, {
            _id: 0,
            __v: 0,
          });
        });

        test('then it should return a post', () => {
          expect(post).toEqual(postStub());
        });
      });
    });

    describe('find', () => {
      describe('when find is called', () => {
        let posts: Post[];

        beforeEach(async () => {
          jest.spyOn(postModel, 'find');
          posts = await postsRepository.find(postFilterQuery);
        });

        test('then it should call the postModel', () => {
          expect(postModel.find).toHaveBeenCalledWith(postFilterQuery);
        });

        test('then it should return a post', () => {
          expect(posts).toEqual([postStub()]);
        });
      });
    });

    describe('findOneAndUpdate', () => {
      describe('when findOneAndUpdate is called', () => {
        let post: Post;

        beforeEach(async () => {
          jest.spyOn(postModel, 'findOneAndUpdate');
          post = await postsRepository.findOneAndUpdate(
            postFilterQuery,
            postStub(),
          );
        });

        test('then it should call the postModel', () => {
          expect(postModel.findOneAndUpdate).toHaveBeenCalledWith(
            postFilterQuery,
            postStub(),
            { new: true },
          );
        });

        test('then it should return a post', () => {
          expect(post).toEqual(postStub());
        });
      });
    });
  });

  describe('create operations', () => {
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          PostsRepository,
          {
            provide: getModelToken(Post.name),
            useValue: PostModel,
          },
        ],
      }).compile();

      postsRepository = moduleRef.get<PostsRepository>(PostsRepository);
    });

    describe('create', () => {
      describe('when create is called', () => {
        let post: Post;
        let saveSpy: jest.SpyInstance;
        let constructorSpy: jest.SpyInstance;

        beforeEach(async () => {
          saveSpy = jest.spyOn(PostModel.prototype, 'save');
          constructorSpy = jest.spyOn(PostModel.prototype, 'constructorSpy');
          post = await postsRepository.create(postStub());
        });

        test('then it should call the postModel', () => {
          expect(saveSpy).toHaveBeenCalled();
          expect(constructorSpy).toHaveBeenCalledWith(postStub());
        });

        test('then it should return a post', () => {
          expect(post).toEqual(postStub());
        });
      });
    });
  });
});
