import { Test } from '@nestjs/testing';
import { PostsController } from '../posts.controller';
import { PostsService } from '../posts.service';
import { CreatePostDto } from '../dto/create-post.dto';
import { Post } from '../schemas/post.schema';
import { postStub } from './stubs/post.stub';
import { UpdatePostDto } from '../dto/update-post.dto';

jest.mock('../posts.service');

describe('PostsController', () => {
  let postsController: PostsController;
  let postsService: PostsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [PostsController],
      providers: [PostsService],
    }).compile();

    postsService = moduleRef.get<PostsService>(PostsService);
    postsController = moduleRef.get<PostsController>(PostsController);
    jest.clearAllMocks();
  });

  describe('create', () => {
    describe('when create is called', () => {
      let post: Post;
      let createPostDto: CreatePostDto;

      beforeEach(async () => {
        createPostDto = {
          status: postStub().status,
          modified_at: postStub().modified_at,
          created_at: postStub().created_at,
          title: postStub().title,
          author: postStub().author,
          url: postStub().url,
          points: postStub().points,
          _highlightResult: undefined,
          _tags: postStub()._tags,
          created_at_i: postStub().created_at_i,
          num_comments: postStub().num_comments,
          story_title: postStub().story_title,
          parent_id: postStub().parent_id,
          story_url: postStub().story_url,
          story_id: postStub().story_id,
          story_text: postStub().story_text,
          objectID: postStub().objectID,
          comment_text: postStub().comment_text,
        };
        post = await postsService.create(createPostDto);
      });

      test('then it should call postsService', () => {
        expect(postsService.create).toHaveBeenCalledWith(createPostDto);
      });

      test('then it should return a post', () => {
        expect(post).toEqual(postStub());
      });
    });
  });

  describe('findAll', () => {
    describe('when findAll is called', () => {
      let post: Post[];
      beforeEach(async () => {
        post = await postsController.findAll();
      });
      test('then it should call postsService', () => {
        expect(postsService.findAll).toHaveBeenCalled();
      });
      test('then it should return a list of posts', () => {
        expect(post).toEqual([postStub()]);
      });
    });
  });

  describe('findOne', () => {
    describe('when findOne is called', () => {
      let post: Post;
      beforeEach(async () => {
        post = await postsController.findOne(postStub().objectID);
      });
      test('then it should call postsService', () => {
        expect(postsService.findOne).toHaveBeenCalledWith(postStub().objectID);
      });
      test('then it should return a post', () => {
        expect(post).toEqual(postStub());
      });
    });
  });

  describe('patch', () => {
    describe('when patch is called', () => {
      let post: Post;
      let updatePostDto: UpdatePostDto;

      beforeEach(async () => {
        updatePostDto = {
          status: 'deleted',
        };
        post = await postsController.patch(postStub().objectID, updatePostDto);
      });

      test('then it should call postsService', () => {
        expect(postsService.patch).toHaveBeenCalledWith(
          postStub().objectID,
          updatePostDto,
        );
      });

      test('then it should return a post', () => {
        expect(post).toEqual(postStub());
      });
    });
  });

  describe('update', () => {
    describe('when update is called', () => {
      let post: Post;
      let updatePostDto: UpdatePostDto;

      beforeEach(async () => {
        updatePostDto = {
          status: 'modified',
        };
        post = await postsController.update(postStub().objectID, updatePostDto);
      });

      test('then it should call postsService', () => {
        expect(postsService.update).toHaveBeenCalledWith(
          postStub().objectID,
          updatePostDto,
        );
      });

      test('then it should return a post', () => {
        expect(post).toEqual(postStub());
      });
    });
  });

  describe('remove', () => {
    describe('when remove is called', () => {
      let post: Post;

      beforeEach(async () => {
        post = await postsController.remove(postStub().objectID);
      });

      test('then it should call postsService', () => {
        expect(postsService.remove).toHaveBeenCalledWith(postStub().objectID);
      });

      test('then it should return a post', () => {
        expect(post).toEqual(postStub());
      });
    });
  });
});
