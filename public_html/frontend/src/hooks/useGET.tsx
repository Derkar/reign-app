import {useEffect, useReducer, useRef, useState} from "react";
import axios, {AxiosResponse} from "axios";

/**
 * This reducer will handle the different events of the request's state.
 * @param state Current state of the reducer hook / request state.
 * @param action The event dispatched when the Reducer Hook was triggered. The action has a Type (INIT, SUCCESS, ERROR) and a Payload (JSON with information)
 * @constructor
 */
const GETReducer = (state: any, action: { type: any; payload?: any; }) => {
    switch (action.type) {
        case `INIT`:
            return {
                status: "LOADING",
                data: null,
                isLoading: true,
                error: false,
            };
        case `SUCCESS`:
            return {
                status: "SUCCESS",
                isLoading: false,
                data: action.payload,
                error: false,
            };
        case `ERROR`:
            return {
                status: "ERROR",
                data: null,
                isLoading: false,
                error: action.payload,
            };
        default:
            throw new Error(`Incorrect action type in GETReducer`);
    }
};

/**
 * Makes a GET Request to an endpoint.
 * forceupdate is useful for updating frontend lists after a POST, PUT, DELETE or PATCH Request.
 * @param url The URL of the endpoint to be requested.
 * @returns an array of elements that contain the data, the state of the request and a function to make the request again.
 */
const useGET = (
    url: string
): [data: any, error: boolean | any, isLoading: boolean, forceUpdate: Function] => {
    // Reducer hook that handles events regarding the overall status of the Request
    const [state, dispatch] = useReducer(GETReducer, {
        status: "IDLE",
        data: null,
        error: false,
        isLoading: true,
    });

    // useState that holds the value of forceUpdate.
    // Used as a dependency for the useEffect hook so it can be fired again later on.
    const [forceUpdated, setForceUpdate] = useState(0);

    // Function that modifies dependency of the useEffect hook
    function forceUpdate() {
        setForceUpdate(forceUpdated + 1);
    }

    // useRef hook that will hold the status of the Request across its complete lifetime
    const statusRef = useRef("");

    // useEffect hook that handles the GET Request.
    // Handles loading, errors and success, and dispatches the appropriate events
    useEffect(() => {
        // Holds the value of the source to be cancelled, if necessary.
        let source = axios.CancelToken.source();
        if (url) {
            if (statusRef.current !== "LOADING") {
                dispatch({type: `INIT`});
                statusRef.current = "LOADING";
            }
            // Makes the GET Request and cancels the previous token for this request.
            axios
                .get(url, {cancelToken: source.token})
                .then((response: AxiosResponse) => {
                    statusRef.current = "IDLE";
                    dispatch({type: `SUCCESS`, payload: response.data});
                })
                .catch(error => {
                    // Handles and event if the Request was NOT cancelled
                    // If the request was cancelled, nothing happens.
                    if (!axios.isCancel(error)) {
                        statusRef.current = "IDLE";
                        dispatch({
                            type: `ERROR`,
                            payload: error.response ? error.response : "ERROR",
                        });
                    }
                });
        }
        return () => {
            source.cancel(`Clean up Cancel`);
        };
    }, [url, forceUpdated]);

    // Returns the values obtained through the Request lifecycle
    return [state.data, state.error, state.isLoading, forceUpdate];
};

export default useGET;
