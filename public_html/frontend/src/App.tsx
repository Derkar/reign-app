import './App.css';
import Axios from "axios";
import {useEffect, useState} from "react";
import useGET from "./hooks/useGET";
import {Post, shortPost, shortPosts} from "./types/posts";
import moment from "moment";
import DeleteIcon from "./components/icons/DeleteIcon";

function App() {
    // useState hook that stores the values of the post data after it is finally sanitized
    const [postsList, setPostsList] = useState<shortPosts>([]);

    // Custom Hook that executes GET Requests to the designated URL
    const [posts, postsError, loadingPosts, forceUpdate] = useGET(`http://localhost/posts`);

    // useEffect hook that handles the response.
    // Depends on the results of custom hook useGET to trigger.
    // Filters and sorts the result list from the GET Request
    useEffect(() => {
        // Only if we have data, have no Errors and are not Loading
        if (posts && !postsError && !loadingPosts) {
            let tempList: shortPosts = posts
                // Filtering posts that were deleted using the trash icon and those with no title at all
                .filter((object: Post) => object.status!=="deleted" && (object.story_title || object.title))
                // Returning the necessary information
                .map((object: Post): shortPost => {
                    return {
                        id: object._id,
                        title: object.story_title ?? object.title,
                        author: object.author,
                        date: object.created_at,
                        url: object.story_url ?? object.url,
                    }
                })
                // Sorting the list by most recent Date
                .sort((a: shortPost, b: shortPost) => {
                    return new Date(b.date).getTime() - new Date(a.date).getTime()
                });
            // Saving the temp List into the hook stored value
            setPostsList(tempList);
        }
    }, [posts, postsError, loadingPosts]);

    /**
     * Minor implementation of a date formatter using momentjs library.
     * @param date The date to be formatted.
     * @returns
     *          The formatted date.
     */
    const formatDate = (date: string) => {
        // Variables to be used. One for today's date and another for the post date
        let postDate = new Date(date);
        let today = new Date();
        // If the post was made today, then...
        if (postDate.getDay() === today.getDay()) {
            // Return the value as 12:32 am (00:32 in 24 hours format)
            return moment(postDate).format("LT");
        // Otherwise, if the post was made yesterday, then....
        } else if (postDate.getDate() === today.getDate() - 1) {
            // Could use either, but momentjs library usage seemed better in my opinion
            //return "Yesterday";
            return moment(postDate).fromNow();
        // Otherwise, if the post was made during the last 7 days, then...
        } else if (today.getDate() - 7 < postDate.getDate() && postDate.getDate() < today.getDate() - 1) {
            // Return the value as Saturday, Monday, and so on.
            return moment(postDate).format('dddd');
        // Otherwise, if the post was made during this year, then...
        } else if (postDate.getFullYear() === today.getFullYear()) {
            // Return the value as Aug 1st, Apr 22nd, Mar 19th
            return moment().format("MMM Do");
        // In any other case...
        } else {
            // Return the value as Aug 1st 2020, Apr 22nd 2020, Mar 19th 1990
            return moment().format("MMM Do YYYY");
        }
    }

    /**
     * Async function that executes a PATCH Request to "delete" the selected post.
     * Instead of erasing the post, it will be hidden, so it won't appear again if the app is restarted.
     * Uses the forceUpdate custom implementation that triggers a GET Request to refresh the data.
     * @param id The ID of the post to be hidden.
     */
    async function deletePost(id: string) {
        if (window.confirm('Do you really want to delete this post?')){
            await Axios.patch(`http://localhost/posts/${id}`, {
                status: "deleted",
            })
                .then(res => res.status === 200 ? forceUpdate() : null)
                .catch(e => console.error(e));
        }
    }

    return (
        <div>
            <header
                className={"Header"}
            >
                <h1
                    style={{marginLeft: "4%"}}
                >
                    HN Feed
                </h1>
                <h5
                    style={{marginLeft: "4%"}}
                >
                    {"We <3 hacker news!"}
                </h5>
            </header>
            <table className={"Table"}>
                <tbody>
                {loadingPosts
                    ? (
                        <tr className={"TableRow"}>
                            <td>Data is loading.</td>
                        </tr>
                    )
                    : postsError
                        ? (
                            <tr className={"TableRow"}>
                                <td>There was an error Fetching the Data.</td>
                            </tr>
                        )
                        : posts && posts.length>0
                            ? postsList.map(post => (
                                <tr
                                    className={"TableRow"}
                                    key={`tr-${post.id}`}
                                >
                                    <td
                                        className={"Col col-xl-9 col-l-9 col-m-9 col-s-9 col-xs-12"}
                                        key={`title-${post.id}`}
                                        onClick={() => {
                                            window.open(post.url);
                                        }}
                                    >
                                    <span
                                        className={"Title"}
                                        key={`title-title-${post.id}`}
                                    >
                                        {post.title}
                                    </span>
                                        <span
                                            className={"Author"}
                                            key={`title-author-${post.id}`}
                                            style={{paddingLeft: "5px", paddingRight: "5px"}}
                                        >
                                        {` - ${post.author} - `}
                                    </span>
                                    </td>
                                    <td
                                        className={"Col col-xl-2 col-l-2 col-m-2 col-s-2 col-xs-12"}
                                        key={`date-${post.id}`}
                                        style={{justifyContent: "center", textAlign: "center"}}
                                        onClick={() => {
                                            window.open(post.url);
                                        }}
                                    >
                                    <span
                                        className={"Date"}
                                        key={`date-date-${post.id}`}
                                    >
                                        {formatDate(post.date)}
                                    </span>
                                    </td>
                                    <td
                                        className={"Col col-xl-1 col-l-1 col-m-1 col-s-1 col-xs-12 Trash"}
                                        style={{justifyContent: "flex-end"}}
                                        key={`delete-${post.id}`}
                                        onClick={() => {
                                            deletePost(post.id);
                                        }}
                                    >
                                        <DeleteIcon/>
                                    </td>
                                </tr>
                            ))
                            : (
                                <div className={"TableRow"}>
                                    <span>There are no registered posts.</span>
                                </div>
                            )
                }
                </tbody>
            </table>
        </div>
    );
}

export default App;
