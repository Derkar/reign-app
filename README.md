# Reign Fullstack Developer Application

This project is a small demo for the task assigned by the staff at [reign.cl](https://www.reign.cl/).

## The App

This project is a monorepo for a basic demo that consists in a frontend component, a backend component and a database component.

### Frontend

The frontend component was bootstrapped using [Create React App](https://github.com/facebook/create-react-app). \
The docker instance was built using [node](https://hub.docker.com/_/node) LTS (Long Term Support) Docker Image. \
The frontend will run on the port [8080](http://localhost:8080) and will make requests to [localhost:80](http://localhost:80), where the backend will be listening.

### Backend

The backend component was bootstrapped using [Nest](https://github.com/nestjs/nest). \
The docker instance was built using [node](https://hub.docker.com/_/node) LTS (Long Term Support) Docker Image. \
The backend will run on the port [80](http://localhost:80) and will connect to [192.168.2.15:27017](http://192.162.2.15:27017), where the MongoDB instance will be listening.

### Database

The database instance is based on [MongoDB](https://www.mongodb.com/). \
MongoDB will be dockerized and hosted with docker internal IP and port [192.168.2.15:27017](http://192.162.2.15:27017). 

## Installation

You must have [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/) installed locally in order for it to work.

```bash
$ docker-compose up
```

Wait for the components to load and then visit the browser at [localhost:8080](http://localhost:8080).

## npm installation

The project can be run using [npm](https://www.npmjs.com/)  (must have [nodejs](https://nodejs.org/) installed previously). \
In order to start the components using npm libraries you must access the component directory.

```bash
$ cd public_html/frontend
```
or
```bash
$ cd public_html/backend
```
appropriately, then run

```bash
$ npm install && npm run
```

## Available Scripts (Globally)

Those scripts were mainly designed for CICD integrations with GitlabCI.

### `npm build`

Builds both backend and frontend.

### `npm test`

Runs unit testing for both backend and frontend. (frontend unit testing WIP)

### `npm lint`

Runs lint for both backend and frontend.

# To the recruiters

I took some liberties when I was designing the frontend.
* I added a message to either confirm or cancel the action before deleting a post. I was planning on creating a more elaborated message, but I didn't have enough time, given the restrictions to not using UI libraries (personally I wanted to use [styled-components](https://styled-components.com/), since i dont feel it's a complete UI). In the end I decided to keep the development as fast as possible.
* The date shown uses the [momentjs](https://momentjs.com/) library. A powerful library that parses and converts Date objects into human-friendly readable words. Keep an eye on the library used, since it's not [momentjs/timezone](https://momentjs.com/timezone/), and will parse Date objects using the machine locale timezone. This was an extra consideration, since I thought it would be best for the end user to see the time using their timezone. Besides, the implementation was faster than creating my own function.
* I was planning on creating a JSX Table component, but it seemed like it was not necessary since I would have instantiated it only once. Again, in the end, I chose to keep development as fast as possible and opted for the HTML table element + css, which achieved the same purpose.
#### Considerations:
* There's no need to run a script to consume HN API in order to populate the database for the demo. I used [NestJS Task Scheduling](https://docs.nestjs.com/techniques/task-scheduling), a powerful library that allows for cron jobs, scheduled tasks, triggers that are timeouts, and so on. The script to populate the DB runs 5 seconds after the backend finished starting up, and every 60 minutes since the server has started. This way the API keeps getting consumed, until the backend server is shut down.
* One thing I didn't manage to complete was to secure MongoDB. There's a specific user the demo uses but since the port is exposed, and Mongo isn't secured, there's no authentication needed in order to access the database.
* Instructions said that posts deleted should not appear again. To solve this, I decided to take one of the fields for every object of the list the HN API was returning  (```objectid``` specifically), and label it as a ```unique``` key. This way, only one post was going to be saved, and I was assured to not have duplicates. In case a dupe would be detected, it would be logged in the backend console and the request would return a ```409 Conflict``` status code. By the way, posts won't be deleted, only hidden.

Cheers!